import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private alertController: AlertController) { }

  async presentAlert(title: string, messageToClient: string) {
    const alert = await this.alertController.create({
      header: title,
      message: messageToClient,
      buttons: ['OK']
    });

    await alert.present();
  }

  async showMessageOkCancel(title: string, message: string) {
    let choice: any;
    const alert = await this.alertController.create({
      header: title,
      subHeader: message,
      buttons: [{
        text: 'Si',
        handler: () => {
          alert.dismiss(true);
          return false;
        }
      }, {
        text: 'No',
        handler: () => {
          alert.dismiss(false);
          return false;
        }
      }]
    });

    await alert.present();
    await alert.onDidDismiss().then((data) => {
      choice = data;
    });
    return choice;
  }

}
