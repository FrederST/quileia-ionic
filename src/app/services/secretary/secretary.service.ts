import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Secretary } from 'src/app/models/Secretary';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SecretaryService {

  urlSecretary = environment.apiURL + '/secretary';

  constructor(private http: HttpClient) { }

  getAllSecretarys(): Observable<Secretary[]> {
    return this.http.get<Secretary[]>(this.urlSecretary + '/secretarys');
  }

  getSecretary(secretaryId: number): Observable<Secretary> {
    return this.http.get<Secretary>(this.urlSecretary + '/' + secretaryId);
  }

  createSecretary(secretary: Secretary): Observable<Secretary>{
    return this.http.post<Secretary>(this.urlSecretary + '/create-secretary', secretary);
  }

  editSecretary(secretary: Secretary): Observable<Secretary> {
    return this.http.put<Secretary>(this.urlSecretary + '/edit-secretary', secretary);
  }

  deleteSecretary(secretaryId: number): Observable<object> {
    return this.http.delete(this.urlSecretary + '/delete-secretary/' + secretaryId);
  }

}
