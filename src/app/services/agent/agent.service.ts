import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { InformationAgentComponent } from 'src/app/components/agent/information-agent/information-agent.component';
import { Agent } from 'src/app/models/Agent';
import { ResponseMessage } from 'src/app/models/ResponseMessage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  urlAgent = environment.apiURL + '/agent';

  constructor(private http: HttpClient, private modalController: ModalController) { }

  getAllAgents(): Observable<Agent[]> {
    return this.http.get<Agent[]>(this.urlAgent + '/agents');
  }

  createAgent(agent: Agent): Observable<Agent> {
    return this.http.post<Agent>(this.urlAgent + '/create-agent', agent);
  }

  editAgent(agent: Agent): Observable<Agent | ResponseMessage> {
    return this.http.put<Agent | ResponseMessage>(this.urlAgent + '/edit-agent', agent);
  }

  deleteAgent(agentId: number): Observable<object> {
    return this.http.delete(this.urlAgent + '/delete-agent/' + agentId);
  }

  async showAgent(agentDf: Agent) {
    const modal = await this.modalController.create({
      component: InformationAgentComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        agent: agentDf
      }
    });
    return await modal.present();
  }

}
