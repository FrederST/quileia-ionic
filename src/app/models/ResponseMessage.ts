export interface ResponseMessage {
    timestamp: Date;
    message: string;
    details: string;
}
