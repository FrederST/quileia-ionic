import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HistoryTrackPage } from './history-track.page';

describe('HistoryTrackPage', () => {
  let component: HistoryTrackPage;
  let fixture: ComponentFixture<HistoryTrackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryTrackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HistoryTrackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
