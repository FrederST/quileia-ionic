import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Track } from 'src/app/models/Track';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { TrackService } from 'src/app/services/track/track.service';

@Component({
  selector: 'app-create-track',
  templateUrl: './create-track.page.html',
  styleUrls: ['./create-track.page.scss'],
})
export class CreateTrackPage implements OnInit {

  createTrackForm: FormGroup;
  showLoader: boolean;
  isEditable = false;
  track: Track = { id: null, typeT: null, streetOrCarrerT: null, numberT: null, levelCongestionT: null };
  constructor(
    private router: Router,
    private trackService: TrackService,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.createTrackForm = this.formBuilder.group({
      typeT: new FormControl('', [Validators.required]),
      streetOrCarrerT: new FormControl('', [Validators.required]),
      numberT: new FormControl('', [Validators.required]),
      levelCongestionT: new FormControl('', [Validators.required])
    });

    if (this.activatedRoute.snapshot.params.editTrack !== undefined) {
      this.createTrackForm.patchValue(JSON.parse(this.activatedRoute.snapshot.params.editTrack));
      this.track = JSON.parse(this.activatedRoute.snapshot.params.editTrack);
      this.isEditable = true;
    }

  }

  showProgressBar() {
    this.showLoader = true;
  }

  hideProgressBar() {
    this.showLoader = false;
  }

  createTrack(): void {
    this.showProgressBar();
    this.track = this.createTrackForm.value;
    this.trackService.createTrack(this.track).subscribe(() => {
      this.dialogService.presentAlert('Mensaje', 'Vía Creada');
      this.hideProgressBar();
      this.router.navigate(['tabs', 'tab2']);
    }, (error) => {
      console.log(error);
    });
  }

  editTrack(): void {
    this.showProgressBar();
    this.track = this.createTrackForm.value;
    this.track.id = JSON.parse(this.activatedRoute.snapshot.params.editTrack).id;

    this.trackService.editTrack(this.track).subscribe(() => {
      this.dialogService.presentAlert('Mensaje', 'Vía Editada');
      this.hideProgressBar();
      this.router.navigate(['tabs', 'tab2']);
    }, (error) => {
      console.log(error);
    });
  }

}
