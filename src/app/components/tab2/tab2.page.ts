import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSearchbar } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Track } from 'src/app/models/Track';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { TrackService } from 'src/app/services/track/track.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  dbTracks: Observable<Track[]>;
  @ViewChild('searchBar') searchBar: IonSearchbar;
  showSearch = false;
  textSearch = '';

  constructor(
    private router: Router,
    private trackService: TrackService,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.dbTracks = this.trackService.getAllTracks();
  }

  showSearchbar(): void {
    this.showSearch = true;
    setTimeout(() => {
      this.searchBar.setFocus();
    }, 500);
  }

  hideSearchbar(): void {
    this.showSearch = false;
  }

  searchTrack(event: any): void  {
    this.textSearch = event.detail.value;
  }

  viewCreateTrack(): void {
    this.router.navigate(['tabs', 'tab2', 'create-track']);
  }

  viewEditTrack(track: Track): void {
    this.router.navigate(['tabs', 'tab2', 'create-track', { editTrack: JSON.stringify(track) }]);
  }

  viewHistoryTrack(track: Track): void {
    this.router.navigate(['tabs', 'tab2', 'history-track', { track: JSON.stringify(track) }]);
  }

  deletetrack(trackId: number, index: number): void {

    this.dialogService.showMessageOkCancel('Advertencia', 'Seguto que desea eliminar este Vía ?')
    .then((confirmed) => {
      if (confirmed.data) {
        this.trackService.deleteTrack(trackId).subscribe(() => {
          this.dialogService.presentAlert('Mensaje', 'Vía Eliminada');
        }, (error) => {
          console.log(error);
        });
      }
    });
  }

}
