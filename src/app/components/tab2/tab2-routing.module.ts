import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTrackPage } from '../track/create-track/create-track.page';
import { HistoryTrackPage } from '../track/history-track/history-track.page';
import { Tab2Page } from './tab2.page';

const routes: Routes = [
  {
    path: '',
    component: Tab2Page,
  },
  {
    path: 'create-track',
    component: CreateTrackPage
  },
  {
    path: 'history-track',
    component: HistoryTrackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
