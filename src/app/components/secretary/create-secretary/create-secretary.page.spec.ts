import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateSecretaryPage } from './create-secretary.page';

describe('CreateSecretaryPage', () => {
  let component: CreateSecretaryPage;
  let fixture: ComponentFixture<CreateSecretaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSecretaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateSecretaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
