import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Secretary } from 'src/app/models/Secretary';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { SecretaryService } from 'src/app/services/secretary/secretary.service';

@Component({
  selector: 'app-create-secretary',
  templateUrl: './create-secretary.page.html',
  styleUrls: ['./create-secretary.page.scss'],
})
export class CreateSecretaryPage implements OnInit {

  createSecretaryForm: FormGroup;
  showLoader: boolean;

  isEditable = false;
  secretary: Secretary = { id: null, nameS: null };
  constructor(
    private router: Router,
    private secretaryService: SecretaryService,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {

    this.createSecretaryForm = this.formBuilder.group({
      nameS: new FormControl('', [Validators.required])
    });

    if (this.activatedRoute.snapshot.params.editSecretary !== undefined) {
      this.createSecretaryForm.patchValue(JSON.parse(this.activatedRoute.snapshot.params.editSecretary));
      this.secretary = JSON.parse(this.activatedRoute.snapshot.params.editSecretary);
      this.isEditable = true;
    }

  }

  showProgressBar() {
    this.showLoader = true;
  }

  hideProgressBar() {
    this.showLoader = false;
  }

  createSecretary(): void {
    this.showProgressBar();
    this.secretary = this.createSecretaryForm.value;
    this.secretaryService.createSecretary(this.secretary).subscribe(() => {
      this.dialogService.presentAlert('Mensaje', 'Secretaria Creada Satisfactoriamente');
      this.hideProgressBar();
      this.router.navigate(['tabs', 'tab4']);
    }, (error) => {
      console.log(error);
    });
  }

  editSecretary(): void {
    this.showProgressBar();
    this.secretary = this.createSecretaryForm.value;
    this.secretary.id = JSON.parse(this.activatedRoute.snapshot.params.editSecretary).id;

    this.secretaryService.editSecretary(this.secretary).subscribe(() => {
      this.dialogService.presentAlert('Mensaje', 'Secretaria Editada Satisfactoriamente');
      this.hideProgressBar();
      this.router.navigate(['tabs', 'tab4']);
    }, (error) => {
      alert(error);
      console.log(error);
    });
  }

}
