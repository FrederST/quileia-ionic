import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateSecretaryPageRoutingModule } from './create-secretary-routing.module';

import { CreateSecretaryPage } from './create-secretary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateSecretaryPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateSecretaryPage]
})
export class CreateSecretaryPageModule {}
