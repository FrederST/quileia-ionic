import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Agent } from 'src/app/models/Agent';
import { HistoryAssignation } from 'src/app/models/HistoryAssignation';
import { HistoryService } from 'src/app/services/history/history.service';

@Component({
  selector: 'app-history-agent',
  templateUrl: './history-agent.page.html',
  styleUrls: ['./history-agent.page.scss'],
})
export class HistoryAgentPage implements OnInit {

  dbHistory: Observable<HistoryAssignation[]>;
  dbAgent: Agent;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private historyService: HistoryService
  ) { }

  ngOnInit(): void {

    if (this.activatedRoute.snapshot.params.agent !== undefined) {
      this.dbAgent = JSON.parse(this.activatedRoute.snapshot.params.agent);
      this.dbHistory = this.historyService.getHistoryOfAgent(this.dbAgent.id);
    }

  }

}
