import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Agent } from 'src/app/models/Agent';
import { ResponseMessage } from 'src/app/models/ResponseMessage';
import { Secretary } from 'src/app/models/Secretary';
import { Track } from 'src/app/models/Track';
import { AgentService } from 'src/app/services/agent/agent.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { SecretaryService } from 'src/app/services/secretary/secretary.service';
import { TrackService } from 'src/app/services/track/track.service';

@Component({
  selector: 'app-create-agent',
  templateUrl: './create-agent.page.html',
  styleUrls: ['./create-agent.page.scss'],
})
export class CreateAgentPage implements OnInit {

  createAgentForm: FormGroup;

  errorMessage: ResponseMessage = { timestamp: null, message: null, details: null };
  dbSecretarys: Observable<Secretary[]>;
  dbTracks: Observable<Track[]>;
  isEditable = false;
  showLoader: boolean;

  agent: Agent = { id: null, nameA: null, lastNameA: null, yearsExperienceA: null, secretaryIdA: null, trackIdA: null };

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private agentService: AgentService,
    private secretaryService: SecretaryService,
    private trackService: TrackService,
    private dialogService: DialogService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

    this.createAgentForm = this.formBuilder.group({
      nameA: new FormControl('', [Validators.required]),
      lastNameA: new FormControl('', [Validators.required]),
      years: new FormControl(0, [Validators.required, Validators.min(0), Validators.max(40)]),
      months: new FormControl(0, [Validators.required, Validators.min(0), Validators.max(12)]),
      secretaryIdA: new FormControl('', [Validators.required]),
      trackIdA: new FormControl('', [Validators.required])
    });

    this.dbSecretarys = this.secretaryService.getAllSecretarys();

    this.dbTracks = this.trackService.getAllAllowedTracks();

    if (this.activatedRoute.snapshot.params.editAgent !== undefined) {
      const tempAgent = JSON.parse(this.activatedRoute.snapshot.params.editAgent);

      this.setYearsAndMonths(tempAgent.yearsExperienceA);
      setTimeout(() => {
        this.createAgentForm.patchValue(tempAgent);
      }, 900);
      this.isEditable = true;
    }

  }

  showProgressBar() {
    this.showLoader = true;
  }

  hideProgressBar() {
    this.showLoader = false;
  }

  createAgent(): void {
    this.showProgressBar();
    this.agent = this.createAgentForm.value;
    this.agent.yearsExperienceA = this.createAgentForm.value.years + this.createAgentForm.value.months / 12;

    this.agentService.createAgent(this.agent).subscribe(() => {
      this.dialogService.presentAlert('Información', 'Agente Creado');
      this.hideProgressBar();
      this.router.navigate(['tabs', 'tab1']);
    }, (error) => {
      alert(error);
    });
  }

  editAgent(): void {
    this.showProgressBar();
    this.agent = this.createAgentForm.value;
    this.agent.yearsExperienceA = this.createAgentForm.value.years + this.createAgentForm.value.months / 12;
    this.agent.id = JSON.parse(this.activatedRoute.snapshot.params.editAgent).id;

    this.agentService.editAgent(this.agent).subscribe(() => {
      this.dialogService.presentAlert('Información', 'Agente Editado');
      this.hideProgressBar();
      this.router.navigate(['tabs', 'tab1']);
    }, (error) => {
      this.errorMessage = {
        timestamp: error.error.timestamp,
        message: error.error.message,
        details: error.error.details
      };
      this.dialogService.presentAlert('Advertencia', this.errorMessage.message);
    });
  }

  private setYearsAndMonths(yearsExperience: number): void {
    const yearsFloat = parseFloat(yearsExperience.toString());

    this.createAgentForm.patchValue({
      years: yearsFloat.toFixed()
    });

    if (yearsFloat.toString().split('.')[1] !== undefined) {
      const decimal = parseFloat('0.' + (yearsFloat + '').split('.')[1]) * 12;
      this.createAgentForm.patchValue({
        months: Math.round(Number(decimal))
      });
    }
  }

}
