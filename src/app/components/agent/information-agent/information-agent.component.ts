import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Agent } from 'src/app/models/Agent';
import { Secretary } from 'src/app/models/Secretary';
import { Track } from 'src/app/models/Track';
import { SecretaryService } from 'src/app/services/secretary/secretary.service';
import { TrackService } from 'src/app/services/track/track.service';

@Component({
  selector: 'app-information-agent',
  templateUrl: './information-agent.component.html',
  styleUrls: ['./information-agent.component.scss'],
})
export class InformationAgentComponent implements OnInit {

  @Input() agent: Agent;
  track: Track;
  secretary: Secretary;
  years: number;
  months: number;

  constructor(
    private modalController: ModalController,
    private secretaryService: SecretaryService,
    private trackService: TrackService
  ) { }

  ngOnInit(): void {
    this.setYearsAndMonths(this.agent.yearsExperienceA);
    this.secretaryService.getSecretary(this.agent.secretaryIdA).subscribe((secretary) => {
      this.secretary = secretary;
    });
    this.trackService.getTrack(this.agent.trackIdA).subscribe((track) => {
      this.track = track;
    });
  }

  dismiss() {
    this.modalController.dismiss({
      dismissed: true
    });
  }

  private setYearsAndMonths(yearsExperience: number): void {
    const yearsFloat = parseFloat(yearsExperience.toString());

    this.years = Number(yearsFloat.toFixed());


    if (yearsFloat.toString().split('.')[1] !== undefined) {
      const decimal = parseFloat('0.' + (yearsFloat + '').split('.')[1]) * 12;
      this.months = Math.round(Number(decimal));
    }
  }

}
