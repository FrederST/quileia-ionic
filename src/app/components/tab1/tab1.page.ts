import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSearchbar } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Agent } from 'src/app/models/Agent';
import { AgentService } from 'src/app/services/agent/agent.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  @ViewChild('searchBar') searchBar: IonSearchbar;
  dbAgents: Observable<Agent[]>;
  showSearch = false;
  textSearch = '';

  constructor(
    private agentService: AgentService,
    private router: Router,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.hideSearchbar();
    this.dbAgents = this.agentService.getAllAgents();
  }

  showSearchbar(): void {
    this.showSearch = true;
    setTimeout(() => {
      this.searchBar.setFocus();
    }, 500);
  }

  hideSearchbar(): void {
    this.showSearch = false;
  }

  searchAgent(event: any): void  {
    this.textSearch = event.detail.value;
  }

  showAgent(agent: Agent): void {
    this.agentService.showAgent(agent);
  }

  viewCreateAgent(): void {
    this.router.navigate(['tabs', 'tab1', 'create-agent']);
  }

  viewhistoryAgent(agetn: Agent): void{
    this.router.navigate(['tabs', 'tab1', 'history-agent', { agent: JSON.stringify(agetn) }]);
  }

  viewEditAgent(agent: Agent): void {
    this.router.navigate(['tabs', 'tab1', 'create-agent', { editAgent: JSON.stringify(agent) }]);
  }

  deleteAgent(agentId: number, index: number): void {

    this.dialogService.showMessageOkCancel('Advertencia', 'Seguto que desea eliminar este agente ?')
    .then((confirmed) => {
      if (confirmed.data) {
        this.agentService.deleteAgent(agentId).subscribe(() => {
          this.dialogService.presentAlert('Mensaje', 'Agente Eliminado');
        }, (error) => {
          console.log(error);
        });
      }
    });
  }

}
