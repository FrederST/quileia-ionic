import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSearchbar } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Secretary } from 'src/app/models/Secretary';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { SecretaryService } from 'src/app/services/secretary/secretary.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  dbSecretarys: Observable<Secretary[]>;
  @ViewChild('searchBar') searchBar: IonSearchbar;
  showSearch = false;
  textSearch = '';

  constructor(
    private secretaryService: SecretaryService,
    private router: Router,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.dbSecretarys = this.secretaryService.getAllSecretarys();
  }

  showSearchbar(): void {
    this.showSearch = true;
    setTimeout(() => {
      this.searchBar.setFocus();
    }, 500);
  }

  hideSearchbar(): void {
    this.showSearch = false;
  }

  searchSecretary(event: any): void  {
    this.textSearch = event.detail.value;
  }

  viewCreateSecretary(): void {
    this.router.navigate(['tabs', 'tab4', 'create-scretary']);
  }

  viewEditSecretary(secretary: Secretary): void {
    this.router.navigate(['tabs', 'tab4', 'create-scretary', { editSecretary: JSON.stringify(secretary) }]);
  }

  deletesecretary(scretaryId: number, index: number): void {

    this.dialogService.showMessageOkCancel('Advertencia', 'Seguto que desea eliminar este secretaria ?')
    .then((confirmed) => {
      if (confirmed.data) {
        this.secretaryService.deleteSecretary(scretaryId).subscribe(() => {
          this.dialogService.presentAlert('Mensaje', 'Secretaria Eliminada');
        }, (error) => {
          console.log(error);
        });
      }
    });
  }

}
