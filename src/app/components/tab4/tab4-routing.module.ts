import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateSecretaryPage } from '../secretary/create-secretary/create-secretary.page';

import { Tab4Page } from './tab4.page';

const routes: Routes = [
  {
    path: '',
    component: Tab4Page
  },
  {
    path: 'create-scretary',
    component: CreateSecretaryPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Tab4PageRoutingModule {}
