import { Pipe, PipeTransform } from '@angular/core';
import { Secretary } from '../models/Secretary';

@Pipe({
  name: 'filterSecretary'
})
export class FilterSecretaryPipe implements PipeTransform {

  transform(secretarys: Secretary[], text: string): Secretary[] {

    if (text.length === 0) {
      return secretarys;
    }

    text = text.toLowerCase();

    if (secretarys.length > 0) {
      return secretarys.filter((secretary) => {
        return secretary.nameS.toLowerCase().includes(text)
        || secretary.id.toString() === text;
      });
    }

    return secretarys;

  }

}
